package main

import (
	"github.com/pkg/browser"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"gitlab.com/jshsj/rechnungen/invoice"
	"net/http"
	"strconv"
)

func main() {

	//inv := invoice.Invoice{
	//	Headline:         "Rechnung",
	//	Number:           25,
	//	Date:             time.Now(),
	//	DaysToPay:        30,
	//	RecipientName:    "Heuschrecke Naturkost GmbH",
	//	RecipientAddress: "Redcar Straße 50a",
	//	RecipientCity:    "10551 Troisdorf",
	//	JobDescription:   nil,
	//	Total:            41616,
	//	Notes:            nil,
	//	Phrase:           "Vielen Dank.",
	//	Name:             "Joshua",
	//	ContactName:      "Joshua Stübner",
	//	ContactAddress:   "Emdener Straße 35",
	//	ContactCity:      "10551 Berlin",
	//	ContactPhone:     "+49 (0) 152 52481201",
	//	ContactMail:      "kontakt@joshuastuebner.com",
	//	ContactBankName:  "DKB",
	//	ContactIBAN:      "3123213",
	//	ContactBIC:       "3123213",
	//	ContactUST:       "21312312",
	//}
	//
	//invoice.Create(inv)

	e := echo.New()
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"http://localhost:3000"},
		AllowMethods: []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete, http.MethodPatch},
	}))
	e.Use(middleware.BodyDump(func(c echo.Context, reqBody, resBody []byte) {
	}))

	go browser.OpenURL("http://localhost:8080")

	e.Static("/", "ui")

	e.GET("/api/invoices", listInvoices)
	e.GET("/api/invoices/new", newInvoice)
	e.GET("/api/invoices/:invoice_number", getInvoice)
	e.POST("/api/invoices", createInvoice)
	e.GET("/api/templates", listTemplates)
	e.GET("/api/templates/:template_name", getTemplate)
	e.POST("/api/templates", createTemplate)


	e.Logger.Fatal(e.Start(":8080"))
}

// return all invoices
func listInvoices(c echo.Context) error {
	c.Response().Header().Set(echo.HeaderAccessControlAllowOrigin, "http://localhost:3000")
	allInvoices := invoice.ListAll()
	return c.JSON(200, allInvoices)
}

// Return empty invoice
func newInvoice(c echo.Context) error {
	c.Response().Header().Set(echo.HeaderAccessControlAllowOrigin, "http://localhost:3000")
	newInvoice := invoice.New()
	return c.JSON(http.StatusOK, newInvoice)
}

// write invoice
func createInvoice(c echo.Context) error {
	c.Response().Header().Set(echo.HeaderAccessControlAllowOrigin, "http://localhost:3000")

	//m := echo.Map{}
	//if err := c.Bind(&m); err != nil {
	//	return err
	//}
	//
	//fmt.Println("%+v\n", m)
	//return c.JSON(http.StatusOK, m)

	i := new(invoice.Invoice)

	if err := c.Bind(i); err != nil {
		return c.NoContent(http.StatusNotAcceptable)
	}

	name, inv := invoice.Create(*i)

	resp := map[string]interface{}{
		"name": name,
		"invoice": inv,
	}
	return c.JSON(http.StatusOK, resp)
}

// return invoice
func getInvoice(c echo.Context) error {
	c.Response().Header().Set(echo.HeaderAccessControlAllowOrigin, "http://localhost:3000")
	number := c.Param("invoice_number")
	num, err := strconv.Atoi(number)
	if err!= nil {
		return c.NoContent(http.StatusBadRequest)
	}

	inv := invoice.Get(num)

	return c.JSON(http.StatusOK, inv)
}

func listTemplates(c echo.Context) error {
	c.Response().Header().Set(echo.HeaderAccessControlAllowOrigin, "http://localhost:3000")
	allTemplates := invoice.ListTemplates()
	return c.JSON(200, allTemplates)
}

// return prefilled invoice

func getTemplate(c echo.Context) error {
	c.Response().Header().Set(echo.HeaderAccessControlAllowOrigin, "http://localhost:3000")
	name := c.Param("template_name")
	template := invoice.GetTemplate(name)
	return c.JSON(200, template)
}

type TemplateResponse struct {
	Name string `json:"templateName"`
	Template invoice.Invoice `json:"template"`
}

// write template
func createTemplate(c echo.Context) error {
	c.Response().Header().Set(echo.HeaderAccessControlAllowOrigin, "http://localhost:3000")
	t := new(TemplateResponse)

	if err := c.Bind(t); err != nil {
		return c.NoContent(http.StatusNotAcceptable)
	}

	name := invoice.CreateTemplate(t.Name, t.Template)

	return c.JSON(http.StatusOK, map[string]string{
		"templateName": name,
	})
}
