package invoice

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"path/filepath"
	"sort"
	"time"
)

type Invoice struct {
	Headline string `json:"headline"`
	Number int `json:"number"`
	Date time.Time `json:"date"`
	DaysToPay int `json:"daysToPay"`
	RecipientName string `json:"recipientName"`
	RecipientAddress string `json:"recipientAddress"`
	RecipientCity string `json:"recipientCity"`
	Jobs []JobDescription `json:"jobs"`
	Total int `json:"total"`
	Notes []Note `json:"notes"`
	Phrase string `json:"phrase"`
	Name string `json:"name"`
	ContactName string `json:"contactName"`
	ContactAddress string `json:"contactAddress"`
	ContactCity string `json:"contactCity"`
	ContactPhone string `json:"contactPhone"`
	ContactMail string `json:"contactMail"`
	ContactBankName string `json:"contactBankName"`
	ContactIBAN string `json:"contactIBAN"`
	ContactBIC string `json:"contactBIC"`
	ContactUST string `json:"contactUST"`
}

type JobDescription struct {
	ID int `json:"id"`
	Name string `json:"name"`
	Text string `json:"text"`
	Options string `json:"options"`
}

type Note struct {
	ID int `json:"id"`
	Text string `json:"text"'`
}

func New() Invoice {
	return Invoice{
		Headline:         "",
		Number:           0,
		Date: 			time.Now(),
		DaysToPay: 			0,
		RecipientName:    "",
		RecipientAddress: "",
		RecipientCity:    "",
		Jobs:   		  nil,
		Total:            0,
		Notes:            nil,
		Phrase:           "",
		Name:             "",
		ContactName:      "",
		ContactAddress:   "",
		ContactCity:      "",
		ContactPhone:     "",
		ContactMail:      "",
		ContactBankName:  "",
		ContactIBAN:      "",
		ContactBIC:       "",
		ContactUST:       "",
	}
}

func ListAll() []Invoice {
	var allInvoices []Invoice

	dirContent, err := ioutil.ReadDir("data")
	if err != nil {
		log.Fatal("Can't read invoice dir.")
	}

	for _, filename := range dirContent {
		name := filepath.Join("data/" + filename.Name())
		if filename.Name() == ".keep" {
			continue
		}
		var invoice Invoice
		file, err := ioutil.ReadFile(name)
		if err != nil {
			log.Fatal("Can't read invoice " + name)
		}
		err = json.Unmarshal(file, &invoice)

		if err!=nil {
			log.Fatal("Can't parse invoice %s, error: %v", name, err)
		}

		allInvoices = append(allInvoices, invoice)
	}

	return allInvoices
}

func Create(invoice Invoice) (string, Invoice) {
	if invoice.Date.IsZero() {
		invoice.Date = time.Now()
	}

	if invoice.Jobs != nil {
		invoice = reassignJobIDs(invoice)
	}

	if invoice.Notes != nil {
		invoice = reassignNoteIDs(invoice)
	}

	file, err := json.MarshalIndent(invoice, "", " ")
	if err != nil {
		log.Fatal("Can't create json.")
	}

	filename := makeFilename(invoice.Number)
	err = ioutil.WriteFile(filepath.Join("data/"+filename+".json"), file, 0644)

	if err!= nil {
		log.Fatal("Can't write file " + filename)
	}

	return filename, invoice
}

func Get(number int) Invoice {
	filename := makeFilename(number)
	file, err := ioutil.ReadFile(filepath.Join("data/" + filename + ".json"))
	if err != nil {
		log.Fatal("Can't read file " + filename)
	}

	var invoice Invoice
	err = json.Unmarshal(file, &invoice)

	if err != nil {
		log.Fatal("Can't parse file " + filename)
	}

	return invoice
}


func makeFilename(number int) string {
	return fmt.Sprintf("rechnung_%d", number)
}

func reassignJobIDs(invoice Invoice) Invoice {
	sort.Sort(ById(invoice.Jobs))

	for i, job := range (invoice.Jobs) {
		job.ID = i
	}
	return invoice
}


type ById []JobDescription

func (a ById) Len() int { return len(a) }
func (a ById) Swap(i, j int) { a[i], a[j] = a[j], a [i]}
func (a ById) Less(i, j int) bool { return a[i].ID < a[j].ID }

func reassignNoteIDs(invoice Invoice) Invoice {
	sort.Sort(NoteById(invoice.Notes))

	for i, note := range (invoice.Notes) {
		note.ID = i
	}
	return invoice
}


type NoteById []Note

func (a NoteById) Len() int { return len(a) }
func (a NoteById) Swap(i, j int) { a[i], a[j] = a[j], a [i]}
func (a NoteById) Less(i, j int) bool { return a[i].ID < a[j].ID }