package invoice

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"path/filepath"
	"strings"
)

type Template struct {
	Name string `json:"templateName"`
	Template Invoice `json:"template"`
}
func ListTemplates() []Template {
	var allTemplates []Template

	dirContent, err := ioutil.ReadDir("templates")
	if err != nil {
		log.Fatal("Can't read template dir.")
	}

	for _, filename := range dirContent {
		name := filepath.Join("templates/" + filename.Name())
		if filename.Name() == ".keep" {
			continue
		}
		var template Template
		file, err := ioutil.ReadFile(name)
		if err != nil {
			log.Fatal("Can't read template " + name)
		}
		err = json.Unmarshal(file, &template.Template)

		if err!=nil {
			log.Fatalf("Can't parse invoice %s, error: %v", name, err)

		}

		template.Name = strings.Split(filename.Name(), ".json")[0]

		allTemplates = append(allTemplates, template)
	}

	return allTemplates

}
func CreateTemplate(name string, invoice Invoice) string {
	file, err := json.MarshalIndent(invoice, "", " ")
	if err != nil {
		log.Fatal("Can't create json.")
	}

	filename := makeTemplateName(name)

	err = ioutil.WriteFile(filepath.Join("templates/"+filename+".json"), file, 0644)

	if err!= nil {
		log.Fatal("Can't write template file for " + invoice.RecipientName)
	}

	return filename
}

func GetTemplate(name string) Invoice {

	file, err := ioutil.ReadFile(filepath.Join("templates/" + name + ".json"))
		if err != nil {
		log.Fatal("Can't read template " + name)
	}

		var invoice Invoice
		err = json.Unmarshal(file, &invoice)

		if err != nil {
		log.Fatal("Can't parse template " + name)
	}

		return invoice
}

func makeTemplateName(name string) string {
	return strings.ToLower(strings.Trim(name, " "))
}