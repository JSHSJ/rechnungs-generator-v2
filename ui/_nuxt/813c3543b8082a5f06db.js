(window.webpackJsonp = window.webpackJsonp || []).push([
  [4],
  {
    185: function (t, e, n) {
      var content = n(190);
      'string' == typeof content && (content = [[t.i, content, '']]), content.locals && (t.exports = content.locals);
      (0, n(56).default)('bd53902a', content, !0, { sourceMap: !1 });
    },
    188: function (t, e) {
      t.exports =
        'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIAAAACACAYAAADDPmHLAAAACXBIWXMAAA7FAAAOxQFHbOz/AAAAGXRFWHRTb2Z0d2FyZQB3d3cuaW5rc2NhcGUub3Jnm+48GgAAAkJJREFUeJzt3b9qFFEUgPFzMjEBiaRxm91mrRKwE5t9CjsrX8GXEQv7+AK+gkLs8grxDwks24gSNWL2WgWSGDAmw9zMfN+vvCwzB/Zj5xZ3dyMkSZIkSZIkSZIkSdLQZO0BTt2fPDjOiLXac1zX4vf3jZjPj2rP8b9Wag+gugwAzgDgVmsPcCqj7JWSd2rPcRUZsRUZG+cWm6ZUGudGbs0msE9G4+luZM7Ori0O9tcj4lelka7NRwCcAcAZAJwBwBkAnAHAGQCcAcAZAJwBwBkAnAHAGQCcAcAZAJwBwBkAnAHAGQDcrTkU2icfX32+ZLU57nSIUl7efbJ8ftPL+AkAZwBwBgDnHqAlJcpeRHb25ZBciU9tXMcAWvLh53L28KlfDFHPGACcAcAZAJwBwBkAnAHAGQCcAcAZAJwBwBkAnAHAGQCcAcAZAJwBwBkAnAHAGQCcAcAZAJwBwBkAnAHAGQCcAcAZAJwBwBkAnAHAGQDcIH4g4uhNPC7L2OzqfqWUzcxh/OnqIALIbF5kE7N/v1IX+QiAMwA4A4AbxB6gRHndRLzt6n477+49+/ojJ2fXvs2/dHX7Vg1jK9ux0Xi6G5nnNp2Lg/31CH8mTj1jAHAGAGcAcAYAZwBwBgBnAHAGAGcAcAYAZwBwBgBnAHAGAGcAcAYAZwBwBgBnAHAGANfasfDRZPq+lBzEMfMr2P5rZTxejcPD3p0Kbu0NK5GPMmOtrev1zslJL4/Y+wiAMwA4A5AkSZIkSZIkSZIkSRqYP17BQwohAWS7AAAAAElFTkSuQmCC';
    },
    189: function (t, e, n) {
      'use strict';
      var o = n(185);
      n.n(o).a;
    },
    190: function (t, e, n) {
      (t.exports = n(55)(!1)).push([
        t.i,
        '.print-container[data-v-db3dc448]{font-family:Lato,sans-serif;width:100vw;height:100vh;position:relative;overflow:hidden;margin:0;padding:0;font-size:14px;color:#151b21;background:#161c22}@media print{body[data-v-db3dc448]{background:none}}*[data-v-db3dc448]{box-sizing:border-box}div[data-v-db3dc448]{margin:0}div[data-v-db3dc448],p[data-v-db3dc448]{padding:0}p[data-v-db3dc448]{margin:0 0 10px;position:relative;width:100%;word-wrap:break-word}h1[data-v-db3dc448]{transform:rotate(90deg);transform-origin:0 100%;position:absolute;bottom:100%;padding:0;margin:0;font-size:32px}.wrap[data-v-db3dc448]{height:100%;width:100%;max-width:210mm;margin:0 auto;padding:9mm 21mm 15mm;background:#fff}.main[data-v-db3dc448]{top:5%;width:100%;height:90%;padding:0}.entry[data-v-db3dc448],.main[data-v-db3dc448]{position:relative}.entry[data-v-db3dc448]{width:50%;left:50%;margin-bottom:35px}.entry[data-v-db3dc448]:before{font-family:Fira Mono,sans-serif;content:attr(data-label);position:absolute;width:150px;text-align:right;left:-210px;top:2px;font-size:12px}.recipient p[data-v-db3dc448]{margin-bottom:0}p[data-v-db3dc448]:before{font-family:Fira Mono,sans-serif;content:attr(data-label);position:absolute;width:150px;text-align:right;left:-210px;top:2px;font-size:12px}.total p span[data-v-db3dc448]{border-bottom:4px solid #f6b203}.big[data-v-db3dc448]{font-size:18px}.total p[data-v-db3dc448]{margin-top:10px;font-size:16px;font-weight:700}.numbers p[data-v-db3dc448]{margin:0}.bold[data-v-db3dc448]{font-weight:700}.contact[data-v-db3dc448]{position:absolute;bottom:0}.contact[data-v-db3dc448],.contact p[data-v-db3dc448]{margin-bottom:0}.logo[data-v-db3dc448]{width:80px;position:absolute;left:0;bottom:0}.edit-container[data-v-db3dc448]{position:absolute;bottom:50px;right:50px;display:flex;flex-direction:column;z-index:100;width:-webkit-max-content;width:-moz-max-content;width:max-content;background:#fff;padding:16px 24px;box-shadow:0 10px 20px rgba(0,0,0,.19),0 6px 6px rgba(0,0,0,.23)}@media print{.edit-container[data-v-db3dc448]{display:none}}.edit-container .action-button[data-v-db3dc448]{width:100%}.save-template[data-v-db3dc448]{margin-top:40px}',
        '',
      ]);
    },
    196: function (t, e, n) {
      'use strict';
      n.r(e);
      n(57), n(78), n(109), n(110), n(17), n(58);
      var o = {
          data: function () {
            return { templateName: '' };
          },
          computed: {
            invoice: function () {
              return this.$store.state.invoices.activeInvoice;
            },
            invoiceDate: function () {
              return new Date(this.invoice.date);
            },
            payDate: function () {
              var t = new Date(this.invoice.date);
              return t.setDate(t.getDate() + this.invoice.daysToPay), t;
            },
            calculatedTotal: function () {
              return (this.invoice.total / 100).toString().replace('.', ',');
            },
          },
          methods: {
            formatDate: function (t) {
              var e = t.getMonth() + 1;
              return (
                e < 10 && (e = '0' + e),
                ''
                  .concat(('0' + t.getDate()).slice(-2), '.')
                  .concat(e, '.')
                  .concat(t.getFullYear())
              );
            },
            saveTemplate: function () {
              this.$store.dispatch('templates/create_template', this.templateName);
            },
            getNoteText: function (t) {
              return t.includes('{{toPay}}') && (t = t.replace('{{toPay}}', this.formatDate(this.payDate))), t;
            },
          },
          fetch: function (t) {
            var e = t.store,
              n = t.route;
            return e.commit('invoices/set_active', n.params.number);
          },
        },
        c = (n(189), n(22)),
        component = Object(c.a)(
          o,
          function () {
            var t = this,
              e = t.$createElement,
              o = t._self._c || e;
            return o('div', { staticClass: 'print-container' }, [
              o(
                'div',
                { staticClass: 'edit-container' },
                [
                  o(
                    'nuxt-link',
                    { staticClass: 'action-button', attrs: { to: '/invoices/' + t.invoice.number + '/edit' } },
                    [t._v('Edit')]
                  ),
                  t._v(' '),
                  o('div', { staticClass: 'save-template' }, [
                    o('label', { attrs: { for: 'templatename' } }, [t._v('Template Name')]),
                    t._v(' '),
                    o('input', {
                      directives: [
                        { name: 'model', rawName: 'v-model', value: t.templateName, expression: 'templateName' },
                      ],
                      attrs: { name: 'templatename', type: 'text' },
                      domProps: { value: t.templateName },
                      on: {
                        input: function (e) {
                          e.target.composing || (t.templateName = e.target.value);
                        },
                      },
                    }),
                    t._v(' '),
                    o(
                      'button',
                      {
                        staticClass: 'button-small',
                        on: {
                          click: function (e) {
                            return t.saveTemplate();
                          },
                        },
                      },
                      [t._v('Save Template')]
                    ),
                  ]),
                ],
                1
              ),
              t._v(' '),
              o('div', { staticClass: 'wrap' }, [
                o(
                  'div',
                  { staticClass: 'main' },
                  [
                    o('h1', [t._v(t._s(t.invoice.headline))]),
                    t._v(' '),
                    o('div', { staticClass: 'entry recipient', attrs: { 'data-label': 'Empfänger' } }, [
                      o('p', [t._v(' ' + t._s(t.invoice.recipientName) + ' ')]),
                      t._v(' '),
                      o('p', [t._v(' ' + t._s(t.invoice.recipientAddress) + ' ')]),
                      t._v(' '),
                      o('p', [t._v(' ' + t._s(t.invoice.recipientCity) + ' ')]),
                    ]),
                    t._v(' '),
                    o('div', { staticClass: 'entry numbers' }, [
                      o('p', { attrs: { 'data-label': 'R. Nr' } }, [t._v(t._s(t.invoice.number))]),
                      t._v(' '),
                      o('p', { attrs: { 'data-label': 'Datum' } }, [t._v(t._s(t.formatDate(t.invoiceDate)))]),
                      t._v(' '),
                      o('p', { attrs: { 'data-label': 'Zahlungsziel' } }, [t._v(t._s(t.formatDate(t.payDate)))]),
                    ]),
                    t._v(' '),
                    t._l(t.invoice.jobs, function (e) {
                      return o('div', { key: e.name, staticClass: 'entry', attrs: { 'data-label': e.name } }, [
                        o('p', { class: e.options }, [t._v(t._s(e.text))]),
                      ]);
                    }),
                    t._v(' '),
                    o('div', { staticClass: 'entry total', attrs: { 'data-label': 'Gesamt' } }, [
                      o('p', [o('span', [t._v(' ' + t._s(t.calculatedTotal) + ' Euro')])]),
                    ]),
                    t._v(' '),
                    o(
                      'div',
                      { staticClass: 'entry notes', attrs: { 'data-label': 'Anmerkungen' } },
                      t._l(t.invoice.notes, function (e) {
                        return o('p', { key: e.id }, [t._v(t._s(t.getNoteText(e.text)))]);
                      }),
                      0
                    ),
                    t._v(' '),
                    o('div', { staticClass: 'entry' }, [
                      o('p', [t._v(t._s(t.invoice.phrase))]),
                      t._v(' '),
                      o('p', { staticClass: 'bold' }, [t._v(t._s(t.invoice.name))]),
                    ]),
                    t._v(' '),
                    o('div', { staticClass: 'entry contact' }, [
                      o('p', [t._v(t._s(t.invoice.contactName))]),
                      t._v(' '),
                      o('p', [t._v(t._s(t.invoice.contactAddress))]),
                      t._v(' '),
                      o('p', [t._v(t._s(t.invoice.contactCity))]),
                      t._v(' '),
                      o('br'),
                      t._v(' '),
                      o('p', [t._v(t._s(t.invoice.contactPhone))]),
                      t._v(' '),
                      o('p', [t._v(t._s(t.invoice.contactMail))]),
                      t._v(' '),
                      o('br'),
                      t._v(' '),
                      o('p', [t._v(t._s(t.invoice.contactBankName))]),
                      t._v(' '),
                      o('p', [t._v(t._s(t.invoice.contactIBAN))]),
                      t._v(' '),
                      o('p', [t._v(t._s(t.invoice.contactBIC))]),
                      t._v(' '),
                      o('p', [t._v(t._s(t.invoice.contactUST))]),
                    ]),
                    t._v(' '),
                    o('img', { staticClass: 'logo', attrs: { src: n(188), alt: 'logo' } }),
                  ],
                  2
                ),
              ]),
            ]);
          },
          [],
          !1,
          null,
          'db3dc448',
          null
        );
      e.default = component.exports;
    },
  },
]);
