(window.webpackJsonp = window.webpackJsonp || []).push([
  [2],
  {
    186: function (t, e, n) {
      var content = n(192);
      'string' == typeof content && (content = [[t.i, content, '']]), content.locals && (t.exports = content.locals);
      (0, n(56).default)('2e107364', content, !0, { sourceMap: !1 });
    },
    191: function (t, e, n) {
      'use strict';
      var o = n(186);
      n.n(o).a;
    },
    192: function (t, e, n) {
      (t.exports = n(55)(!1)).push([
        t.i,
        '.invoice-link{margin-bottom:20px;width:100%;display:grid;grid-template-columns:2% 73% 15%;grid-column-gap:2%;font-size:18px;align-items:flex-end}.invoice-number{font-weight:700;color:#161c22;position:relative}.invoice-number:after{width:100%;content:"";height:50%;background:#f7b202;position:absolute;bottom:0;left:0;z-index:-1}.invoice-recipient{color:#161c22;font-size:20px}.invoice-date{color:#595959}',
        '',
      ]);
    },
    197: function (t, e, n) {
      'use strict';
      n.r(e);
      var o = {
          components: {},
          computed: {
            invoices: function () {
              return this.$store.state.invoices.list;
            },
          },
          methods: {
            formatDate: function (t) {
              var e = new Date(t),
                n = e.getMonth() + 1;
              return (
                n < 10 && (n = '0' + n),
                ''
                  .concat(('0' + e.getDate()).slice(-2), '.')
                  .concat(n, '.')
                  .concat(e.getFullYear())
              );
            },
          },
        },
        c = (n(191), n(22)),
        component = Object(c.a)(
          o,
          function () {
            var t = this,
              e = t.$createElement,
              n = t._self._c || e;
            return n(
              'div',
              { staticClass: 'container' },
              [
                n('h1', [t._v('Rechnungen')]),
                t._v(' '),
                t._l(t.invoices, function (e) {
                  return n(
                    'nuxt-link',
                    { key: e.number, staticClass: 'invoice-link', attrs: { to: '/invoices/' + e.number + '/print' } },
                    [
                      n('p', { staticClass: 'invoice-number' }, [t._v(t._s(e.number))]),
                      t._v(' '),
                      n('p', { staticClass: 'invoice-recipient' }, [t._v(t._s(e.recipientName))]),
                      t._v(' '),
                      n('p', { staticClass: 'invoice-date' }, [t._v(t._s(t.formatDate(e.date)))]),
                    ]
                  );
                }),
                t._v(' '),
                n('nuxt-link', { staticClass: 'add-button', attrs: { to: '/new-invoice' } }, [
                  n(
                    'svg',
                    {
                      staticStyle: { 'enable-background': 'new 0 0 357 357' },
                      attrs: {
                        xmlns: 'http://www.w3.org/2000/svg',
                        'xmlns:xlink': 'http://www.w3.org/1999/xlink',
                        version: '1.1',
                        viewBox: '0 0 357 357',
                        'xml:space': 'preserve',
                      },
                    },
                    [
                      n('g', [
                        n('g', [
                          n('g', { attrs: { id: 'add' } }, [
                            n('path', {
                              staticClass: 'active-path',
                              attrs: {
                                d: 'M357,204H204v153h-51V204H0v-51h153V0h51v153h153V204z',
                                'data-original': '#000000',
                              },
                            }),
                          ]),
                        ]),
                      ]),
                    ]
                  ),
                  t._v('New'),
                ]),
              ],
              2
            );
          },
          [],
          !1,
          null,
          null,
          null
        );
      e.default = component.exports;
    },
  },
]);
